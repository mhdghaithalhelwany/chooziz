export function getColorsArray(){
    return [
        "#e040fb",
        "#ff5252",
        "#536dfe",
        "#69f0ae"
    ]
}

export function getRandomColor(){
    let colors = getColorsArray();
    return colors[Math.floor(Math.random() * (colors.length))]
}
