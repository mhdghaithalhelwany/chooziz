<?php


namespace App\Services;


use Illuminate\Http\Request;

class FileService
{

    /**
     * @param null $whereType
     * @return array
     * @throws \League\Flysystem\FileNotFoundException
     */
    public function getAllFilesInfo($whereTypes= []) : array
    {
        $filesNames = \Storage::allFiles('public');
        $files = [];
        foreach($filesNames as $fileName){
            $fileType = \Storage::disk()->getMimeType($fileName);
            foreach($whereTypes as $whereType)
                if($whereType != $fileType)
                    continue;

            $files[] = [
                'name' => explode("/", $fileName)[1],
                "size" => round(\Storage::disk()->size($fileName) / 1024),
                "type" => $fileType,
                "url" => \Storage::url($fileName)
            ];
        }
        return $files;
    }


    /**
     * @param Request $request
     */
    public function putFile(Request $request)
    {
        if($request->has("keep_name"))
            $request->file("file")->storeAs('', $request->file('file')->getClientOriginalName(), ['disk' => 'public']);
        else
            $request->file("file")->store('', ['disk' => 'public']);
    }

    /**
     * @param string $name
     */
    public function deleteFile(string $name)
    {
        unlink(public_path('storage/' . $name));
    }
}