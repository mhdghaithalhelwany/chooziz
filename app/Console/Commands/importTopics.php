<?php

namespace App\Console\Commands;

use App\Services\FileService;
use App\Services\ImportService;
use Illuminate\Console\Command;

class importTopics extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:topics';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import topics form CSV';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $fileService = new FileService();
        $importService = new ImportService();

        $csvFiles = $fileService->getAllFilesInfo(['application/csv', 'text/plain']);
        if(sizeof($csvFiles) == 0) {
            $this->info("No CSV files were found in storage");
            return 0;
        }
        $this->info("Choose CSV file: ");
        foreach($csvFiles as $index => $csvFile)
            $this->info($index + 1 . ': ' . $csvFile['name']);
        $file = $this->ask("Your Choice");

        $errors = $importService->importTopics($csvFiles[$file - 1]['name']);
        if($errors)
            $this->warn("Could not import, error in rows:" . json_encode($errors));
        else
            $this->info("Imported Successfully");
    }
}
