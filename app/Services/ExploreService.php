<?php

namespace App\Services;

use App\Models\Category;
use App\Models\Topic;
use App\Models\View;
use Illuminate\Database\Eloquent\Collection;

class ExploreService
{

    /**
     * @param Category $category
     * @return Collection
     */
    public function getMostViewedTopicsByCategory(Category $category) : Collection
    {
        return Topic::query()
            ->where("category_id", $category->id)
            ->withCount("views")
            ->with("image")
            ->get()
            ->sortByDesc("views_count")
            ->take(config("pagination.most_viewed"))
            ->values();
    }

    /**
     * @return Collection
     */
    public function getMostViewedTopics() : Collection
    {
        $categories = Category::all();
        foreach($categories as $category)
            $category['topics'] = $this->getMostViewedTopicsByCategory($category);
        return $categories;
    }
}