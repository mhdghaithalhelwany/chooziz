<?php


namespace App\Models;


use Illuminate\Support\Str;

trait UsesSlugForUrl
{
    public static function boot()
    {
        parent::boot();
        static::saving(function ($model) {
            $model->slug = Str::slug($model->name);
        });
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }
}