<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Topic extends Model
{
    use HasFactory, SoftDeletes, UsesSlugForUrl;

    protected $fillable = ['name', 'slug', 'description', 'image_id', 'category_id'];
    /**
     * @return HasMany
     */
    public function items() : HasMany
    {
        return $this->hasMany(Item::class);
    }

    /**
     * @return BelongsTo
     */
    public function user() : BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return BelongsTo
     */
    public function image() : BelongsTo
    {
        return $this->belongsTo(Image::class);
    }

    /**
     * @return HasMany
     */
    public function views() : HasMany
    {
        return $this->hasMany(View::class);
    }

    /**
     * @return BelongsTo
     */
    public function category() : BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    public function keywords() : HasMany
    {
        return $this->hasMany(Keyword::class);
    }
}
