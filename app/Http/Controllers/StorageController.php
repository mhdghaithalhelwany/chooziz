<?php

namespace App\Http\Controllers;

use App\Services\FileService;
use Illuminate\Http\Request;
use Inertia\Response;
use Inertia\Inertia;

class StorageController extends Controller
{
    public FileService $fileService;
    public function __construct(FileService $fileService)
    {
        $this->fileService = $fileService;
    }

    /**
     * @param Request $request
     * @return Response
     * @throws \League\Flysystem\FileNotFoundException
     */
    public function index(Request $request) : Response
    {
        return Inertia::render("Dashboard/Storage", [
            'files' => $this->fileService->getAllFilesInfo()
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Routing\Redirector|\Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse
     */
    public function store(Request $request) : \Illuminate\Routing\Redirector|\Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse
    {
        $this->fileService->putFile($request);
        return redirect()->route("storage");
    }

    public function delete(Request $request) //: \Illuminate\Routing\Redirector|\Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse
    {
        $this->fileService->deleteFile($request->name);
        return redirect()->route("storage");
    }
}
