<?php

namespace Tests;

use App\Services\FilterService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use App\Services\ExploreService;
use JetBrains\PhpStorm\Pure;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, RefreshDatabase;

    /**
     * @param $count
     * @param $model
     * @param array $attributes
     * @return mixed
     */
    public function createModelInstances($model, int $count, $attributes = []): mixed
    {
        $factory = $model::factory($attributes);
        if($count > 1)
            $factory = $factory->count($count);
        return $factory->create();
    }

    /**
     * @return ExploreService
     */
    #[Pure] public function getExploreService() : ExploreService
    {
        return new ExploreService();
    }

    /**
     * @return FilterService
     */
    #[Pure] public function getFilterService() : FilterService
    {
        return new FilterService();
    }
}
