<?php

namespace App\Http\Controllers;

use App\Services\ExploreService;
use App\Services\FilterService;
use App\Services\LoggingService;
use Illuminate\Http\Request;
use Inertia\Inertia;

class HomeController extends Controller
{
    public ExploreService $exploreService;
    public FilterService $filterService;
    public LoggingService $loggingService;

    public function __construct(ExploreService $exploreService, FilterService $filterService, LoggingService $loggingService)
    {
        $this->exploreService = $exploreService;
        $this->filterService = $filterService;
        $this->loggingService = $loggingService;
    }

    public function welcome(): \Inertia\Response
    {
        return Inertia::render('Welcome');
    }

    public function filter(Request $request)  : \Inertia\Response
    {
        $searchText = $request->get('searchText');
        if(!$searchText)
            return $this->explore($request);
        $this->loggingService->logSearch($searchText, $request->ip());
        return Inertia::render('Filter', [
            "data" => $this->filterService->filter($searchText),
            "searchText" => $request->get('searchText')
        ]);
    }

    public function explore(Request $request) : \Inertia\Response
    {
        return Inertia::render('Explore', [
            'categories' => $this->exploreService->getMostViewedTopics()
        ]);
    }

    public function dashboard(Request $request) : \Inertia\Response
    {
        return Inertia::render('Dashboard/Dashboard');
    }

}
