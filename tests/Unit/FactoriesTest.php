<?php

namespace Tests\Unit;

use App\Models\Category;
use App\Models\Image;
use App\Models\Item;
use App\Models\Keyword;
use App\Models\Search;
use App\Models\Topic;
use App\Models\User;
use App\Models\View;
use Tests\TestCase;

class FactoriesTest extends TestCase
{
    public function testUserFactory()
    {
        $user = $this->createModelInstances(User::class, 1);
        $this->assertDatabaseHas("users", [
            'id' => $user['id']
        ]);
    }

    public function testItemFactory()
    {
        $topic = $this->createModelInstances(Topic::class, 1);
        $item = $this->createModelInstances(Item::class, 1, [
            'topic_id' => $topic->id,
        ]);
        $this->assertDatabaseHas("items", [
            'id' => $item['id']
        ]);
    }

    public function testImageFactory()
    {
        $image = $this->createModelInstances(Image::class, 1);
        $this->assertDatabaseHas("images", [
            'id' => $image['id']
        ]);
    }

    public function testTopicFactory()
    {
        $topic = $this->createModelInstances(Topic::class, 1);
        $this->assertDatabaseHas("topics", [
            'id' => $topic['id']
        ]);
    }

    public function testCategoryFactory()
    {
        $category = $this->createModelInstances(Category::class, 1);
        $this->assertDatabaseHas("categories", [
            'id' => $category['id']
        ]);
    }

    public function testViewFactory()
    {
        $view = $this->createModelInstances(View::class, 1);
        $this->assertDatabaseHas("views", [
            'id' => $view['id']
        ]);
    }

    public function testSearchFactory()
    {
        $search = $this->createModelInstances(Search::class, 1);
        $this->assertDatabaseHas("searches", [
            'id' => $search['id']
        ]);
    }

    public function testKeywordFactory()
    {
        $keyword = $this->createModelInstances(Keyword::class, 1);
        $this->assertDatabaseHas("keywords", [
            'id' => $keyword['id']
        ]);
    }
}
