<?php


namespace App\Services;

use App\Models\Category;
use App\Models\Image;
use App\Models\Topic;
use Illuminate\Support\Facades\Storage;

class ImportService
{

    public function validateData(array $data)
    {
        $validator = \Validator::make($data, [
            'name' => ['required', 'max:300'],
            'description' => ['required', 'max:1000'],
            'category' => ['required'],
            'image_url' => ['required', "max:500"],
            'image_alt' => ['required', "max:300"],
            'items' => ["array"],
            'keywords' => ["array"],
            "items.*" => ["max: 500"],
            "keywords.*" => ["max: 255"],
        ]);
        if ($validator->fails()) return $validator->errors();
        return false;
    }

    /**
     * @param string $path
     * @return array|bool
     */
    public function importTopics(string $path) : array|bool
    {
        $csv_string = Storage::get("public/" . $path);
        $lines_array = explode("\n", trim($csv_string));
        array_shift($lines_array); //Removes Header
        $data = [];

        $errors = [];
        foreach($lines_array as $index => $line){
            $data_array = str_getcsv($line);
            $data_object = [
                'name' => $data_array[0],
                'description' => $data_array[1],
                'category' => $data_array[2],
                'image_url' => $data_array[3],
                'image_alt' => $data_array[4],
                'items' => $data_array[5],
                'keywords' => $data_array[6],
            ];

            $data_object['items'] = json_decode($data_object['items']);

            if(json_last_error() != JSON_ERROR_NONE)
                $errors[$data_array[0] . " items"] = json_last_error_msg();

            $data_object['keywords'] = json_decode($data_object['keywords']);

            if(json_last_error() != JSON_ERROR_NONE)
                $errors[$data_array[0] . " keywords"] = json_last_error_msg();

            $data[] = $data_object;
        }

        if($errors)
            return $errors;

        foreach($data as $index => $data_object){
            $failed = $this->validateData($data_object);
            if($failed)
                $errors[$data_object['name']] = $failed;
        }

        if($errors)
            return $errors;

        foreach($data as $data_object){
            $this->createTopicFromData($data_object);
        }

        return 0;
    }

    /**
     * @param array $data
     */
    public function createTopicFromData(array $data)
    {
        $items = [];
        $keywords = [];
        foreach($data['items'] as $item)
            $items[] = ["text" => $item];
        foreach($data['keywords'] as $keyword)
            $keywords[] = ["name" => $keyword];

        $data['category_id'] = Category::query()->where("name", $data['category'])->firstOrCreate([
            "name" => $data['category']
        ])['id'];

        $data['image_id'] = Image::create([
            'url' => $data['image_url'],
            'alt' => $data['image_alt']
        ])['id'];

        /* @var \App\Models\Topic $topic */
        $topic = Topic::create($data);
        $topic->items()->createMany($items);
        $topic->keywords()->createMany($keywords);
    }
}