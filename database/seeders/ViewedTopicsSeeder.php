<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Topic;
use App\Models\View;
use Illuminate\Database\Seeder;

class ViewedTopicsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = Category::factory()->count(4)->create();
        foreach ($categories as $category){
            $topics = Topic::factory()->count(random_int(5, 10))->create();
            foreach ($topics as $topic){
                View::factory()->count(random_int(0, 30))->create([
                    "topic_id" => $topic['id']
                ]);
            }
        }
    }
}
