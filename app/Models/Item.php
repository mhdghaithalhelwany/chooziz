<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Item extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = ['text'];

    /**
     * @return BelongsTo
     */
    public function topic() : BelongsTo
    {
        return $this->belongsTo(Topic::class);
    }
}
