<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Category extends Model
{
    use HasFactory, SoftDeletes, UsesSlugForUrl;

    protected $fillable = ['name'];

    /**
     * @return HasMany
     */
    public function topics() : HasMany
    {
        return $this->hasMany(Topic::class);
    }
}
