<?php

namespace Database\Factories;

use App\Models\Topic;
use App\Models\User;
use App\Models\view;
use Illuminate\Database\Eloquent\Factories\Factory;

class ViewFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = view::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $topic_id = $this->faker->randomElement(Topic::all()->pluck("id")->toArray());
        if(!$topic_id)
            $topic_id = Topic::factory()->create()['id'];

        return [
            'topic_id' => $topic_id,
            'ip' => $this->faker->ipv4,
            'type' => random_int(0, 2)
        ];
    }
}
