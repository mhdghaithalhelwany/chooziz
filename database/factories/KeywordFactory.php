<?php

namespace Database\Factories;

use App\Models\Category;
use App\Models\keyword;
use App\Models\Topic;
use Illuminate\Database\Eloquent\Factories\Factory;

class KeywordFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = keyword::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        // The System stinks

        $topic_id = $this->faker->randomElement(Topic::all()->pluck("id")->toArray());
        if(!$topic_id)
            $topic_id = Topic::factory()->create()['id'];

        return [
            'name' => $this->faker->words(5, true),
            'topic_id' => $topic_id
        ];
    }
}
