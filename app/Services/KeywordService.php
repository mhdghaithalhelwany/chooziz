<?php


namespace App\Services;


use App\Models\Topic;

class KeywordService
{
    /**
     * @param Topic $topic
     * @return string
     */
    public function getKeywordsMetaString(Topic $topic) : string
    {
        return $topic->keywords->implode("name", ", ");
    }
}