<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Services\FilterService;
use Illuminate\Http\Request;
use Inertia\Inertia;

class CategoryController extends Controller
{
    public FilterService $filterService;
    public function __construct(FilterService $filterService)
    {
        $this->filterService = $filterService;
    }

    /**
     * @param Request $request
     * @param Category $category
     * @return \Inertia\Response
     */
    public function get(Request $request, Category $category)
    {
        return Inertia::render('Filter', [
            "data" => $this->filterService->filterByCategory($category),
            "searchText" => ''
        ]);
    }
}
