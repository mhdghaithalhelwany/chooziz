<?php


namespace App\Services;


use App\Models\Search;
use App\Models\Topic;
use App\Models\View;

class LoggingService
{
    /**
     * @param Topic $topic
     * @param string $ip
     * @param int $type
     */
    public function logView(Topic $topic, string $ip, int $type)
    {
        $topic->views()->create([
            'ip' => $ip,
            'type' => $type
        ]);
    }

    /**
     * @param string $searchText
     * @param string $ip
     */
    public function logSearch(string $searchText, string $ip)
    {
        Search::create([
            'search_text' => $searchText,
            'ip' => $ip,
        ]);
    }
}