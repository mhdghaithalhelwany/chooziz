<?php

namespace Database\Factories;

use App\Models\Category;
use App\Models\Image;
use App\Models\Item;
use App\Models\Topic;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class TopicFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Topic::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $category_id = $this->faker->randomElement(Category::all()->pluck("id")->toArray());
        if(!$category_id)
            $category_id = Category::factory()->create()['id'];

        $image_id = $this->faker->randomElement(Image::all()->pluck("id")->toArray());
        if(!$image_id)
            $image_id = Image::factory()->create()['id'];

        $user_id = $this->faker->randomElement(User::all()->pluck("id")->toArray());
        if(!$user_id)
            $user_id = User::factory()->create()['id'];

        return [
            'name' => $this->faker->text(50),
            'description' => $this->faker->text(300),
            'category_id' => $category_id,
            'image_id' => $image_id,
        ];
    }

    public function configure()
    {
        return $this->afterCreating(function (Topic $topic) {
            if($topic['status'] == 0)
                return;
            Item::factory()->count(random_int(5, 10))->create([
                'topic_id' => $topic->id,
            ]);
        });
    }
}
