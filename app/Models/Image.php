<?php

namespace App\Models;

use App\Services\FileService;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\App;

class Image extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = ['alt', 'url'];

    /**
     * @return HasOne
     */
    public function topic() : HasOne
    {
        return $this->hasOne(Topic::class);
    }
}
