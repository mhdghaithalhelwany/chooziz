<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTopicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('topics', function (Blueprint $table) {
            $table->id();
            $table->string("name", 300);
            $table->string("slug", 300);
            $table->string("description", 1000);
            $table->unsignedBigInteger("image_id");
            $table->unsignedBigInteger("category_id");

            $table->timestamps();
            $table->softDeletes();
            $table->foreign("image_id")->references('id')->on("images");
            $table->foreign("category_id")->references("id")->on("categories");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('topics');
    }
}
