<?php

namespace Tests\Feature;

use App\Models\Category;
use App\Models\Keyword;
use App\Models\Topic;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class filterServiceTest extends TestCase
{
    public function test_filter_is_working()
    {
        //Given
        $topicsWithAppleKeyword = $this->createModelInstances(Topic::class, 2);
        $topicsWithoutAppleKeyword = $this->createModelInstances(Topic::class, 2);

        foreach($topicsWithAppleKeyword as $topic)
            $this->createModelInstances(Keyword::class, 1, [
                'topic_id' => $topic,
                'name' => 'apple'
            ]);

        //When
        $results = $this->getFilterService()->filter("apple", false)->pluck("relevance", "topic_id");

        //Then
        foreach ($topicsWithAppleKeyword as $topic)
            $this->assertArrayHasKey($topic['id'], $results);
        foreach ($topicsWithoutAppleKeyword as $topic)
            $this->assertArrayNotHasKey($topic['id'], $results);

    }

    public function test_filter_relevance_is_working()
    {
        //Given
        $topicWithAppleKeywordOnce = $this->createModelInstances(Topic::class, 1);
        $topicWithAppleKeywordTwice = $this->createModelInstances(Topic::class, 1);
        $topicWithAppleKeywordThreeTimes = $this->createModelInstances(Topic::class, 1);
        $topicsWithoutAppleKeyword = $this->createModelInstances(Topic::class, 20);

        $this->createModelInstances(Keyword::class, 1, [
            'topic_id' => $topicWithAppleKeywordOnce['id'],
            'name' => 'apple'
        ]);
        $this->createModelInstances(Keyword::class, 2, [
            'topic_id' => $topicWithAppleKeywordTwice['id'],
            'name' => 'apple'
        ]);
        $this->createModelInstances(Keyword::class, 3, [
            'topic_id' => $topicWithAppleKeywordThreeTimes['id'],
            'name' => 'apple'
        ]);

        //When
        $results = $this->getFilterService()->filter("apple", false)->pluck("topic_id")->toArray();
        $expected = [
            $topicWithAppleKeywordThreeTimes['id'],
            $topicWithAppleKeywordTwice['id'],
            $topicWithAppleKeywordOnce['id'],
        ];

        //Then
        foreach ($results as $index => $result)
            $this->assertEquals($expected[$index], $result);
    }


    public function test_filter_by_category()
    {
        //Given
        $category1 = $this->createModelInstances(Category::class, 1);
        $category2 = $this->createModelInstances(Category::class, 1);
        $category1Topics = $this->createModelInstances(Topic::class, 5, [
            'category_id' => $category1['id']
        ]);
        $category2Topics = $this->createModelInstances(Topic::class, 5, [
            'category_id' => $category2['id']
        ]);

        //When
        $result = $this->getFilterService()->filterByCategory($category1, true)->pluck("name", "id");

        //Then
        foreach ($category1Topics as $topic)
            $this->assertArrayHasKey($topic['id'], $result);

        foreach ($category2Topics as $topic)
            $this->assertArrayNotHasKey($topic['id'], $result);
    }
}
