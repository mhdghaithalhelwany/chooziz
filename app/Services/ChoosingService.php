<?php


namespace App\Services;


use App\Models\Topic;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class ChoosingService
{
    /**
     * @param Topic $topic
     * @return HasMany
     */
    private function getItemsByTopicQuery(Topic $topic) : HasMany
    {
        return $topic->items();
    }

    private function getRandomElementFromQuery(mixed $query) : Model
    {
        return $query->inRandomOrder()->first();
    }

    /**
     * @param Topic $topic
     * @return Model
     */
    public function chooseItemFromTopicByRandom(Topic $topic) : Model
    {
        return $this->getRandomElementFromQuery($this->getItemsByTopicQuery($topic));
    }
}