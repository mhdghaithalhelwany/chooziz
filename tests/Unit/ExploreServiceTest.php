<?php


namespace Tests\Unit;


use App\Models\Category;
use App\Models\Topic;
use App\Models\View;
use Tests\TestCase;

class ExploreServiceTest extends TestCase
{

    public function testGetMostViewedTopicsByCategory()
    {
        //Given
        $category = $this->createModelInstances(Category::class, 1);
        $viewedTopics1Time = $this->createModelInstances(Topic::class, 7, ["name" => "1 time", "category_id" => $category->id]);
        $viewedTopics2Times = $this->createModelInstances(Topic::class, 7, ["name" => "2 times", "category_id" => $category->id]);
        $unviewedTopics = $this->createModelInstances(Topic::class, 7, ["name" => "0 times", "category_id" => $category->id]);
        foreach($viewedTopics1Time as $topic){
            $this->createModelInstances(View::class, 1, [
                "topic_id" => $topic['id'],
            ]);
        }
        foreach($viewedTopics2Times as $topic){
            $this->createModelInstances(View::class, 2, [
                "topic_id" => $topic['id'],
            ]);
        }

        //When
        $mostViewedTopics = $this->getExploreService()->getMostViewedTopicsByCategory($category)->pluck("views_count", "id")->toArray();

        //Then
        self::assertEquals(7, sizeof($mostViewedTopics));
        foreach($viewedTopics2Times as $topic)
            $this->assertArrayHasKey($topic['id'], $mostViewedTopics);
        foreach($viewedTopics1Time as $topic)
            $this->assertArrayNotHasKey($topic['id'], $mostViewedTopics);
        foreach($unviewedTopics as $topic)
            $this->assertArrayNotHasKey($topic['id'], $mostViewedTopics);

    }
}