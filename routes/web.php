<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', \App\Http\Controllers\HomeController::class . "@welcome")->name("welcome");
Route::get('/explore', \App\Http\Controllers\HomeController::class . "@explore")->name("explore");
Route::get('/filter', \App\Http\Controllers\HomeController::class . "@filter")->name("filter");
Route::get('/featured', \App\Http\Controllers\HomeController::class . "@featured")->name("featured");

Route::get('/category/{category}', \App\Http\Controllers\CategoryController::class . '@get')->name("category.get");

Route::get('/topic/{topic}', \App\Http\Controllers\TopicController::class . '@get')->name("topic.get");
Route::get('/topic/{topic}/qr', \App\Http\Controllers\TopicController::class . '@getQr')->name("topic.getQr");
Route::get('/topic/{topic}/random', \App\Http\Controllers\TopicController::class . '@getRandom')->name("topic.getRandom");

Route::middleware(['auth', 'verified'])->group(function(){
    Route::get('/dashboard', \App\Http\Controllers\HomeController::class . "@dashboard")->name('dashboard');
    Route::get('/files', \App\Http\Controllers\StorageController::class . '@index')->name("storage");
    Route::post('/files', \App\Http\Controllers\StorageController::class . '@store')->name("storage.store");
    Route::delete('/files', \App\Http\Controllers\StorageController::class . '@delete')->name("storage.delete");
    Route::get('/import', \App\Http\Controllers\StorageController::class . '@import')->name("import");
});

require __DIR__.'/auth.php';
