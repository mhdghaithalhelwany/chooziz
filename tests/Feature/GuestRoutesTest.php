<?php


namespace Tests\Feature;


use Tests\TestCase;

class GuestRoutesTest extends TestCase
{
    public function test_guest_can_access_home_page()
    {
        $response = $this->get('/');
        $response->assertStatus(200);
    }

    public function test_guest_can_access_explore_page()
    {
        $response = $this->get('/explore');
        $response->assertStatus(200);
    }

    public function test_guest_can_access_filter_page()
    {
        $response = $this->get('/filter?searchText=test');
        $response->assertStatus(200);
    }
}