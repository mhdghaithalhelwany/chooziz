<?php

namespace App\Services;

use App\Models\Category;
use App\Models\Keyword;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class FilterService
{

    /**
     * @param string $searchText
     * @param bool $paginate
     * @return LengthAwarePaginator|Collection
     */
    public function filter(string $searchText = "", $paginate = true) : LengthAwarePaginator|Collection
    {
        $query = $this->searchByKeywords($searchText);
        return $paginate ?
            $query->paginate(config("pagination.search"))->appends(['searchText' => $searchText]) :
            $query->get();
    }

    /**
     * Returns Search By Keywords Eloquent Builder Query
     * @param string $searchText
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function searchByKeywords(string $searchText = ""): \Illuminate\Database\Eloquent\Builder
    {
        $query = Keyword::query()
            ->select([DB::raw("count(id) as relevance"), "topic_id"])
            ->groupBy("topic_id")
            ->with("topic")
            ->with("topic.image");
        $searchTerms = explode(" ", $searchText);
        $query->where(function ($query) use ($searchTerms){
            foreach($searchTerms as $searchTerm)
                $query->orWhere("name", "LIKE", "$searchTerm");
        })->orderByDesc("relevance");;
        return $query;
    }

    /**
     * @param Category $category
     * @param bool $paginate
     * @return LengthAwarePaginator|Collection
     */
    public function filterByCategory(Category $category, $paginate = true) : LengthAwarePaginator|Collection
    {
        $query = $category->topics()->with("image");
        return $paginate ?
            $query->paginate(config("pagination.search")) :
            $query->get();
    }
}