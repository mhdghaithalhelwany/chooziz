<?php

return [
    'measurement_id' => env('GOOGLE_ANALYTICS_MEASUREMENT_ID'),
    'script_src' => env('GOOGLE_ANALYTICS_SCRIPT_SRC')
];