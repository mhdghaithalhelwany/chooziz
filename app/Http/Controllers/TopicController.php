<?php

namespace App\Http\Controllers;

use App\Models\Topic;
use App\Services\ChoosingService;
use App\Services\KeywordService;
use App\Services\LoggingService;
use App\Services\QrService;
use Illuminate\Http\Request;
use Inertia\Inertia;

class TopicController extends Controller
{

    public ChoosingService $choosingService;
    public QrService $qrService;
    public KeywordService $keywordService;
    public LoggingService $loggingService;

    public function __construct(ChoosingService $choosingService, QrService $qrService, KeywordService $keywordService, LoggingService $loggingService)
    {
        $this->choosingService = $choosingService;
        $this->qrService = $qrService;
        $this->keywordService = $keywordService;
        $this->loggingService = $loggingService;
    }

    /**
     * @param Topic $topic
     * @param Request $request
     * @return \Inertia\Response
     */
    public function get(Topic $topic, Request $request): \Inertia\Response
    {
        $this->loggingService->logView($topic, $request->ip(), 0);
        $topic->image; // Append image to topic (for image alt)
        return Inertia::render("Topic", [
            'topic' => $topic,
            'keywords' => $this->keywordService->getKeywordsMetaString($topic)
        ]);
    }

    /**
     * @param Topic $topic
     * @param Request $request
     * @return \Inertia\Response
     */
    public function getQr(Topic $topic, Request $request) : \Inertia\Response
    {
        $this->loggingService->logView($topic, $request->ip(), 2);
        return Inertia::render("Qr", [
            'qr' => $this->qrService->generateQrWithLogo(route('topic.getRandom', $topic), $topic['name']),
            'name' => $topic['name'],
            'keywords' => $this->keywordService->getKeywordsMetaString($topic)
        ]);
    }


    /**
     * @param Topic $topic
     * @param Request $request
     * @return \Inertia\Response
     */
    public function getRandom(Topic $topic, Request $request) : \Inertia\Response
    {
        $this->loggingService->logView($topic, $request->ip(), 1);
        return Inertia::render("Random", [
            'topic_name' => $topic['name'],
            'item_name' => $this->choosingService->chooseItemFromTopicByRandom($topic)['text'],
            'keywords' => $this->keywordService->getKeywordsMetaString($topic)
        ]);
    }
}
